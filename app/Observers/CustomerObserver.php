<?php

namespace App\Observers;

use App\Models\Customer;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserDeleteMail;

class CustomerObserver
{
    public function creating(Customer $customer): void
    {
        $customer->uid =  random_int(100000, 999999);
        $customer->remaining_days = random_int(0, 365);
    }

    /**
     * Handle the Customer "created" event.
     */
    public function created(Customer $customer): void
    {
        
    }

    /**
     * Handle the Customer "updated" event.
     */
    public function updated(Customer $customer): void
    {
        //
    }

    /**
     * Handle the Customer "deleted" event.
     */
    public function deleted(Customer $customer): void
    {
        
    }

    /**
     * Handle the Customer "restored" event.
     */
    public function restored(Customer $customer): void
    {
        //
    }

    /**
     * Handle the Customer "force deleted" event.
     */
    public function forceDeleted(Customer $customer): void
    {
        //
    }
}
