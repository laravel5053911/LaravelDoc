<?php

namespace App\Providers;

use App\Observers\CustomerObserver;
use Illuminate\Support\ServiceProvider;
use App\Models\Customer;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Customer::observe(CustomerObserver::class);
    }
}
