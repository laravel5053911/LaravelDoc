<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
Use App\Mail\CustomerRegardMail;

class SendRememberMail extends Command
{
    /**
     * The name and signature of the console command. 
     *
     * @var string
     */
    protected $signature = 'app:send-remember-mail';

    /**
     * The console command description. 
     *
     * @var string
     */
    protected $description = 'Used to convey our thanks to the customers';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        Mail::to('vansh@logisticinfotech.co.in')->send(new CustomerRegardMail());
    }
}
