<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *     title="JWT auth apis",
 *     version="1.0.0",
 *     description="this is the api docs of jwt auth apis made for learning purposes",
 *     @OA\Contact(
 *         name="vansh goswami",
 *         email="vansh@xmail.com"
 *     )
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;
}
