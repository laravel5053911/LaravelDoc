<?php

namespace App\Http\Controllers;

use App\Events\CustomerDeleted;
use Illuminate\Http\Request;
use yajra\DataTables\DataTables;
use App\Models\Customer;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) 
        {
            $userModel = Customer::select();
            
            return DataTables::of($userModel)->make(true);
                    // ->addIndexColumn()
                    // ->addColumn('action', function($row){
                    //     $btn = '<a href="users/edit/' . $row->id . '" class="btn btn-primary btn-sm mx-5">Edit</a>
                    //             <a href="users/delete/'. $row->id . '" class="btn btn-danger btn-sm">Delete</a>';
                    //     return $btn;
                    // })
                    // ->rawColumns(['action'])
                    // ->make(true);
        }
        return view('users.users');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // return view('users.add');
        return view('users.add');   

        // echo "hi";
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'name'       => 'required',
            'email'      => 'required|email',
        ]);

        $user = new Customer;

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->save(); 

        return redirect('users');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $remaining_days = Customer::subscriptiondays($id);

        return view('users.subscription', ['data' => $remaining_days]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {

        $user = Customer::find($id);
        // Debugbar::info($user);
        // echo $user;
        return view('users.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validte = $request->validate([
            'name'       => 'required',
            'email'      => 'required|email',
        ]);

        Customer::find($id)->update($request->except(['_method', '_token']));
        
        // $request->session()->flash('message', 'user updated successfully'); 
        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $user = Customer::find($id);
        event(new CustomerDeleted($user->name, $user->email));
        $user->delete();

        return redirect('users');
    }
}
