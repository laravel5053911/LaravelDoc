<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Controllers\Controller;
use App\Mail\ForgotPasswordMail;
use App\Models\ResetToken;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Mail;

class ForgetPasswordController extends Controller
{
    /**
    * @OA\Post(
    *     path="/api/v2/forget-password",
    *     operationId="v2, Forgot Password",
    *     tags={"Auth"},
    *     summary="Forgot Password",
    *     description="Resets the existing password",
    *     @OA\RequestBody(
    *         required=true,
    *         @OA\JsonContent(
    *             required={"email"},
    *             @OA\Property(property="email", type="string", format="email", example="johndoe@example.com"),
    *         )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Success",
    *         @OA\JsonContent(
    *             @OA\Property(property="status", type="string", example="Success"),
    *             @OA\Property(property="message", type="string", example="E mail has been sent, check email to reset password")
    *         )
    *     ),
    *     @OA\Response(
    *         response=401,
    *         description="Something went wrong",
    *         @OA\JsonContent(
    *             @OA\Property(property="error", type="string", example="Something went wrong"),
    *             @OA\Property(property="message", type="string", example="Error message")
    *         )
    *     )
    * )
    */
    public function index(Request $request) 
    {
        try {

            $user = User::where('email', $request->email)->get();

            if (count($user) > 0)  {
               $token = Str::random(40); 

               $domain = URL::to('/');
               $url = $domain . '/forget-password/' . $token;

               Mail::to($request->email)->queue(new ForgotPasswordMail($url, $request->email));

               $time = Carbon::now()->format('Y-m-d H:i:s');

               ResetToken::updateOrCreate(
                ['email' => $request->email],
                [
                    'email' => $request->email,
                    'token' => $token,
                    'created_at' => $time
                ]);

                return response()->json([
                    'status' => 'success',
                    'message' => 'E mail has been sent, check email to reset password',
                    'time' => $time
                ]); 
                
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'user not found',
                    'time' => Carbon::now()
                ]);
            }

        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function load ($token) 
    {
        $tokenInfo = DB::table('password_reset_tokens')->where('token', $token)->first();

        $userInfo = User::where('email', $tokenInfo->email)->first();

        return view('reset', ['data' => $userInfo]);
    }

    public function reset(Request $request) 
    {
        $request->validate([
            'password' => 'required|string|min:8|confirmed'
        ]);

        $user = User::find($request->id);
        $user->password = $request->password;

        // dd($request);
        $user->save();

        return "password uppdated";
    }
}
