<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Notifications\NewNotification;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
// use yajra\DataTables\DataTables;
use App\Models\User;
use Tymon\JWTAuth\Providers\Auth\Illuminate;
use Validator;

class UserController extends Controller
{
    /**
    * @OA\Post(
    *     path="/api/v1/register",
    *     operationId="v1 register",
    *     tags={"Auth"},
    *     summary="Register a new user",
    *     description="Registers a new user using given data",
    *     @OA\RequestBody(
    *         required=true,
    *         @OA\JsonContent(
    *             required={"name","email","password"},
    *             @OA\Property(property="name", type="string", example="Hello World"),
    *             @OA\Property(property="email", type="string", format="email", example="hello@example.com"),
    *             @OA\Property(property="password", type="string", format="password", example="doejohn@123")
    *         )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Success",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="Success")
    *         )
    *     ),
    *     @OA\Response(
    *         response=401,
    *         description="Something went wrong",
    *         @OA\JsonContent(
    *             @OA\Property(property="error", type="string", example="Something went wrong")
    *         )
    *     )
    * )
    */
    public function register(Request $request)
    {
        $rules = [
            'name'       => 'required',
            'email'      => 'required|email',
            'password' => 'required|min:8'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $validator->errors();
        } else {
            $detail = new User();

            $detail->name = $request->name;
            $detail->email = $request->email;
            $detail->password = $request->password;
            $result = $detail->save();

            return $result ? response()->json(['message' => 'Success']) 
                : response()->json(['error' => 'Something went wrong'], 401);
        }
        
    }

    /**
    * @OA\Post(
    *     path="/api/v1/login",
    *     operationId="v1 login",
    *     tags={"Auth"},
    *     summary="Login",
    *     description="Logs in a user with the provided credentials and returns an access token",
    *     @OA\RequestBody(
    *         required=true,
    *         @OA\JsonContent(
    *             required={"email","password"},
    *             @OA\Property(property="email", type="string", format="email", example="john@example.com"),
    *             @OA\Property(property="password", type="string", format="password", example="password123")
    *         )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Success",
    *         @OA\JsonContent(
    *             @OA\Property(property="access_token", type="string", example="JWT token"),
    *             @OA\Property(property="token_type", type="string", example="Bearer"),
    *             @OA\Property(property="expires_in", type="integer", example="3600")
    *         )
    *     ),
    *     @OA\Response(
    *         response=401,
    *         description="Unauthorized",
    *         @OA\JsonContent(
    *             @OA\Property(property="error", type="string", example="Unauthorized")
    *         )
    *     )
    * )
    */
    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        if (! $token = auth('api')->attempt($credentials)) {

            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
    * @OA\Get(
    *     path="/api/v1/me",
    *     operationId="v1 me",
    *     tags={"User"},
    *     summary="Gets the current authenticated user",
    *     description="Returns information about the authenticated user",
    *     @OA\Response(
    *         response=200,
    *         description="Success",
    *         @OA\JsonContent(
    *             @OA\Property(property="id", type="integer"),
    *             @OA\Property(property="name", type="string"),
    *             @OA\Property(property="email", type="string", format="email"),
    *             @OA\Property(property="created_at", type="string", format="datetime"),
    *             @OA\Property(property="updated_at", type="string", format="datetime")
    *         )
    *     ),
    *     @OA\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function me()
    {
        return response()->json(auth()->user());    
    }

    
    /**
    * @OA\Post(
    *     path="/api/v1/logout",
    *     operationId="v1 logout",
    *     tags={"Auth"},
    *     summary="Logout",
    *     description="Logs out the authenticated user",
    *     @OA\Response(
    *         response=200,
    *         description="Success",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="Successfully logged out")
    *         )
    *     )
    * )
    */
    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
    * @OA\Post(
    *     path="/api/v1/refresh",
    *     operationId="v1 refreshToken",
    *     tags={"Auth"},
    *     summary="Refresh access token",
    *     description="Refreshes the access token using a refresh token",
    *     @OA\Response(
    *         response=200,
    *         description="Success",
    *         @OA\JsonContent(
    *             @OA\Property(property="access_token", type="string", example="New JWT token"),
    *             @OA\Property(property="token_type", type="string", example="Bearer"),
    *             @OA\Property(property="expires_in", type="integer", example="3600")
    *         )
    *     ),
    *     @OA\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }

    /**
    * @OA\Get(
    *    path="/api/v1/list",
    *     operationId="v1 listUsers",
    *     tags={"User"},
    *     summary="Lists all users",
    *     description="Returns a list of all users",
    *     @OA\Response(
    *         response=200,
    *         description="Success",
    *         @OA\JsonContent(
    *             type="array",
    *             @OA\Items(
    *                 type="object",
    *                 @OA\Property(property="id", type="integer", example=1),
    *                 @OA\Property(property="name", type="string", example="vansh"),
    *                 @OA\Property(property="email", type="string", example="vansh@gmail.com"),
    *                 @OA\Property(property="created_at", type="string", example="2024-03-19 13:06:55"),
    *                 @OA\Property(property="updated_at", type="string", example="2024-03-22 13:06:37")
    *             )
    *         )
    *     ),
    *     @OA\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function list() {
        return User::all();
    }

    public function notify() {
        $user = User::find(2);
        $user->notify(new NewNotification($user->name));
        echo "hi";
    }
}
