<?php

namespace App\Listeners;

use App\Events\CustomerDeleted;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserDeleteMail;

class SendDeleteMail implements ShouldQueue
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(CustomerDeleted $event): void
    {
        Mail::to($event->email)->send(new UserDeleteMail($event->name));
    }
}
