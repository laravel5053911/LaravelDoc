<?php

// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\UserController as V1UserController;
use App\Http\Controllers\Api\V2\UserController as V2UserController;
use App\Http\Controllers\Api\V1\ForgetPasswordController as V1FP;
use App\Http\Controllers\Api\V2\ForgetPasswordController as V2FP;


// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:sanctum');


//api Version 1 Routes

Route::group(['prefix' => '/v1',], function ($router) {
    Route::post('logout', [V1UserController::class, 'logout']);
    Route::post('refresh', [V1UserController::class, 'refresh']);
    Route::get('me', [V1UserController::class, 'me']);
    Route::get('list', [V1UserController::class, 'list']);
    Route::post('login', [V1UserController::class, 'login']);
    Route::post('register', [V1UserController::class, 'register']);
    Route::post('forget-password', [V1FP::class, 'index']);
});

//api version 2 routes

Route::group([
    'middleware' => 'auth.api',
    'prefix' => '/v2',
], function ($router) {
    Route::post('logout', [V2UserController::class, 'logout']);
    Route::post('refresh', [V2UserController::class, 'refresh']);
    Route::get('me', [V2UserController::class, 'me']);
    Route::get('list', [V2UserController::class, 'list']);
});

Route::group(['prefix' => '/v2',], function ($router) {
    Route::post('login', [V2UserController::class, 'login']);
    Route::post('register', [V2UserController::class, 'register']);
    Route::post('forget-password', [V2FP::class, 'index']);
});



// in case of unknown routes
Route::any('{any}', function(){
    return response()->json([
    	'status' => 'error',
        'message' => 'Resource not found'], 404);
})->where('any', '.*');

