<?php

use App\Http\Controllers\Api\V1\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\Api\V1\ForgetPasswordController;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Route::get('users', [CustomerController::class, 'index'])->name('users.index')->middleware('ath');  
Route::middleware(['ath'])->group(function () {
    Route::get('users', [CustomerController::class, 'index'])->name('users.index');

    Route::get('users/delete/{id}', [CustomerController::class, 'destroy']);

    Route::get('users/add', [CustomerController::class, 'create'])->name('users.add');

    Route::post('users/save', [CustomerController::class, 'store'])->name('users.store');

    Route::get('users/edit/{id}', [CustomerController::class, 'edit'])->name('users.edit');

    Route::post('users/update/{id}', [CustomerController::class, 'update'])->name('users.update');

    Route::get('users/subscription/{id}', [CustomerController::class, 'show']);

});

Route::get('forget-password/{token}', [ForgetPasswordController::class, 'load']);

Route::post('forget-password', [ForgetPasswordController::class, 'reset']);

Route::get('notify', [UserController::class, 'notify']);

Route::get('lang/{locale}', function ($locale) {

    App::setLocale($locale);

    return view('lang');
});


