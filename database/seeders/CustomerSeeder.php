<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();

        foreach (range(1, 220) as $index) {
            DB::table('customers')->insert([
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail,
                'created_at' =>  $faker->date("Y_m_d H_i_s"),
                'updated_at' =>  $faker->date("Y_m_d H_i_s"),
                'uid' => random_int(100000, 999999),
                'remaining_days' => random_int(0, 365),
            ]);
        }
    }
}
