# Steps for creating virtual hosts for laravel projects in ubuntu

**step-1**: launch your terminal and go to the sites-available directory inside the apache2 using the following command.

>`cd /etc/apache2/sites-available`

**step-2**: copy the contents of 000-default.conf into your project file as shown below.

>`cp 000-default.conf laraveldoc.conf`

**step-3**: open laraveldoc.conf in text editors(recommended: nano) as below.

>`sudo nano larveldoc.conf`

step-4: write the name of the server below the ServerAdmin and give the path to your public folder of the project in DocumentRoot.

	ServerAdmin webmaster@localhost
    ServerName laraveldoc.localhost
	DocumentRoot /var/www/html/LaravelDoc/public

>Note: It would be easier to setup if your ServerName is project-name.localhost and not project-name.com

step-5: Now that we have created the host file we must activate it, we can activate the host using the command as shown below

>`sudo a2ensite laraveldoc.conf`

Step-6: Now we are almost there, to run the project from virtual host you will need give the following storage permissions from your project directory

To give the storage permissions use the below command
>`sudo chmod -R 775 storage`

>`sudo chown -R $USER:www-data storage`

Generate the project key using below command.

> `php artisan key:generate`

Finally, go to your browser and type your host name like `laraveldoc.localhost` and there you have it, your laravel project running from the vortual host insted of `php artisan serve`
