# Laravel Training Document

This project is for learning purposes and made while learning laravel.

## Steps for creating virtual host for laravel projects in ubuntu

step-1: launch your terminal and go to the sites-available directory inside the apache2 using the following command.

>`cd /etc/apache2/sites-available`

step-2: copy the contents of 000-default.conf into your project file as shown below.

>`cp 000-default.conf laraveldoc.conf`

step-3: open laraveldoc.conf in text editors(recommended: nano) as below.

>`sudo nano larveldoc.conf`

step-4: write the name of the server below the ServerAdmin and give the path to your public folder of the project in DocumentRoot.

	ServerAdmin webmaster@localhost
    ServerName laraveldoc.localhost
	DocumentRoot /var/www/html/LaravelDoc/public

>Note: It would be easier to setup if your ServerName is project-name.localhost and not project-name.com

step-5: Now that we have created the host file we must activate it, we can activate the host using the command as shown below

>`sudo a2ensite laraveldoc.conf`

Step-6: To run the project from virtual host you will need give the following permissions from your project directory
To give the storage permissions

>`sudo chmod -R 775 storage`

>`sudo chown -R $USER:www-data storage`

Generate the project key using below command.

> `php artisan key:generate`

Finally go to your browser and type your host name like `laraveldoc.localhost` and there you have it, your laravel project running from the vortual host insted of `php artisan serve`



## steps used while working on the project (all useful comands)

### To crete the env file if .env.example is available

>`cp .env.example .env`


### To create the env file if .env.example file is not available

> `touch .env`


### To generate the storage key

> `php artisan key:generate`


### To give the storage permissions

>`sudo chmod -R 777 bootstrap/cache storage`

>`sudo chown -R $USER:www-data storage`

### Command to clear all the cache of the project

>`php artisan optimize:clear`


### The project is made using the following composer command.(and uses the laravel11)

>`composer create-project laravel/laravel LaravelDoc`


### Command to make new database table using migration
>`php artisan make:migration customers`

Command to add new field(column) inside of an existing db table

>`php artisan make:migration add_uid_to_customers --table="customers"`

Run the below command to migrate the database tables,

>`php artisan migrate`


Lit of packages used in this project

Package Name     |Command to install
-----------------|--------------------------------------
laravel/ui       |`composer require laravel/ui`
fzaninotto/faker |`composer require fzaninotto/faker`
tymon/jwt-auth   |`composer require tymon/jwt-auth`

Run the below command to install node modules

>`npm install` 
or
>`npm i`


then run the below command to get vite

>`npm run dev`


### Command to create seeder(which will be used to seed the Users table)

>`php artisan make:seeder UserSeeder`


Command to seed the database using UserSeeder

>`php artisan db:seed --class=UserSeeder`


Command to create the UserController(this is the resource controller)

>`php aritsan make:controller UserController --resource`


### Command to create the middleware

>`php artisan make:middleware IsAuthenticated`

>in laravel 11 you register middleware in bootstrap/app.php in boot method

### Command to create the mailable class

>`php artisan make:mail UserDeleteMail`

### Command to create the model observer

>`php artisn make:observe UserObserver --model=User`

In laravel 11 you may manually register an observer by invoking the observe method on the model you wish to observe. You may register observers in the boot method of your application's AppServiceProvider class:


### Command to run the worker

>`php artisan queue:work`



 > Note: **Run the migration command before starting the server**

 ### Api authentication using jwt tokens

Run the below command to setup the api.php for routes and other api things for the project

>`php artisan api:install`

We will be using tymon/jwt-auth for this project, so in order to install the package run the following command

>`composer require tymon/jwt-auth`

steps to setup the package:

save the following proviser in `bootstrap/providers.php` 

     <?php

	 return [
		App\Providers\AppServiceProvider::class,
		Yajra\DataTables\DataTablesServiceProvider::class,
		Barryvdh\Debugbar\ServiceProvider::class,
		Tymon\JWTAuth\Providers\LaravelServiceProvider::class,
	];


Run the following command to publish the package config file:

>`php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"`

You should now have a `config/jwt.php` file that allows you to configure the basics of this package.

Now you will need to generate a secret key for jwt, run the following command to generate the secret key

>`php artisan jwt:secret`
this key will be updated in your .env file

while working with the jwt tokens you will need the middleware to protect routes, so register middleware in your `bootstrap/app.php` as shown below.

    $middleware->alias([
        'auth:api' => \Tymon\JWTAuth\Http\Middleware\Authenticate::class,
    ]);


### Steps to generate the api documentation(swagger) using darkaonline

Run the following command to install the l5-swagger package

>`composer require "darkaonline/l5-swagger`

Then run the following command to publish the vendor for the package

>`php artisan vendor:publish --provider "L5Swagger\L5SwaggerServiceProvider`

Run the following command to create the swagger after adding the suitable comments for each api

>`php artisan l5-swagger:generate`


### Steps to generate the event and listener

Run the below command to make the event class

>`php artisan make:event CustomerDeleted`

Run the below command to make the listener for the event

>`php artisan make:listener SendDeleteMail`

### Steps to generate new events and listeners

Run the below command to generate the event class

>`php artisan make:event UserDeleted`

Run the below command to generate the listener which listens for the event

>`php artisan make:listener SendDeleteMail --event=UserDeleted`

### Steps to generate the new artisan command

Run the below command to create the command class

>`php artisan make:command SendWelcomeMail`
then you can write the logic in handle method

### Steps to genetate the NewNotification 

Run the below command to generate the notification file

>`php artisan make:notification NewNotification`









Run the below command to install laravel packages that are used in this project

>`composer update`

then run the below command to get vite

>`npm run dev`

Finally just fire the below command to start the server and get the server up and running

>`php artisan serve`