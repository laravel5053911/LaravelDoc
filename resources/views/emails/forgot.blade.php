<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forgot password mail</title>
</head>
<body>
    <h4>Click on below link to reset password</h4>

    <a href="{{$url}}">Click here to reset password</a>
</body>
</html>