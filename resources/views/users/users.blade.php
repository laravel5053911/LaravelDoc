@extends('layouts.app')

@section('content')
<div class="container">
         <h1 class="mb-5 mt-5">List of users</h1>
         <table class="table table-bordered data-table">
            <thead>
               <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Created At</th>
                  <th>Updated At</th>
                  <th>Action</th>
               </tr>
            </thead>
            <tbody>
            </tbody>
         </table>
      </div>
@endsection

@push("scripts")
      <script type="text/javascript">
         $(function () {
            var table = $('.data-table').DataTable({
               processing: true,
               serverSide: true,
               ajax: "{{ route('users.index') }}",
               columns: [
                  {data: 'id', name: 'id'},
                  {data: 'name', name: 'name'},
                  {data: 'email', name: 'email'},
                  {data: 'created_at', name: 'created_at'},
                  {data: 'updated_at', name: 'updated_at'},
                  {data: null, name: 'action', render: function(row) {
                     return '<a href="users/edit/' + row.id + '" class="edit btn btn-primary mx-3">Edit</a><a href="users/delete/' + row.id +'" class="edit btn btn-danger btn-sm">Delete</a><a href="users/subscription/' + row.id + '" class="btn btn-secondary btn-sm mx-3">Remaining days</a>';
                  }, orderable: false, searchable: false},
               ]
            });
         }); 
      </script>

@endpush