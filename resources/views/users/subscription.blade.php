@extends('layouts.app')

@section('content')

@if($data > 0)
    <h1>Remaining subscription days: {{$data}} </h1>
@else
    <h1>Subscription Over</h1>
@endif

@endsection