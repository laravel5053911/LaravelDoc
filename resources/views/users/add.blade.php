@extends('layouts.app')

@section('content')


<form method="POST" action="{{route('users.store')}}">
    @csrf
  <div class="input-group mb-3">
    <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" name="name">
  </div>
  <div class="form-group">
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="email">
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>

  <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
  <input type="submit" value="Submit" class="btn btn-primary">

</form>

@endsection