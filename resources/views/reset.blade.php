@extends('layouts.app')

@section('content')

@if($errors->any())

<ul>
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>
    @endforeach
</ul>

@endif

<form method="POST" action="/forget-password">
    @csrf
    <input type="hidden" name="id" value="{{$data->id}}">
    Password: <input type="password" name="password"><br><br>
    Confirm Password: <input type="password" name="password_confirmation"><br><br>
    <input type="submit" value="Reset Password">
</form>

@endsection