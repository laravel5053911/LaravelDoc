<?php

return [
    'welcome' => 'Welcome to our application',
    'msg' => 'The town was in flames.
    The narrow streets leading to the moat and the first terrace belched smoke
    and embers, flames devouring the densely clustered thatched houses and
    licking at the castle walls. From the west, from the harbour gate, the screams
    and clamour of vicious battle and the dull blows of a battering ram smashing
    against the walls grew ever louder.
    Their attackers had surrounded them unexpectedly, shattering the
    barricades which had been held by no more than a few soldiers, a handful of
    townsmen carrying halberds and some crossbowmen from the guild. Their
    horses, decked out in flowing black caparisons, flew over the barricades like
    spectres, their riders bright, glistening blades sowing death amongst the
    fleeing defenders.',
];